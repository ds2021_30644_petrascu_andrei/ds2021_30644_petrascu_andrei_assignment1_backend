package ro.tuc.ds2020.dtos;

import lombok.Data;
import ro.tuc.ds2020.entities.Device;

import javax.persistence.OneToOne;
import java.util.UUID;

@Data
public class SensorDTO {
    private UUID id;
    private String description;
    private Integer maxValue;
    private Integer currentValue;
    private Device device;
}
