package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.entities.Person;

public class PersonBuilder {

    public static Person toEntity(PersonDTO personDTO) {
        Person person = new Person();
        person.setId(personDTO.getId());
        person.setUsername(personDTO.getUsername());
        person.setPassword(personDTO.getPassword());
        person.setName(personDTO.getName());
        person.setBirth(personDTO.getBirth());
        person.setAddress(personDTO.getAddress());
        person.setRole(personDTO.getRole());
        return person;
    }

    public static PersonDTO toDTO(Person person) {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setId(person.getId());
        personDTO.setUsername(person.getUsername());
        personDTO.setPassword(person.getPassword());
        personDTO.setName(person.getName());
        personDTO.setBirth(person.getBirth());
        personDTO.setAddress(person.getAddress());
        personDTO.setRole(person.getRole());
        return personDTO;
    }


}
