package ro.tuc.ds2020.dtos.builders;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.EnergyDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Energy;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.SensorRepository;

@Component
@RequiredArgsConstructor
public class EnergyBuilder {


    public static EnergyDTO toDTO(Energy energy) {
        return null;
    }

    public static Energy toEntity(EnergyDTO energyDTO, Sensor sensor) {
       Energy energy = new Energy();
       energy.setId(energyDTO.getId());
       energy.setConsumption(energyDTO.getConsumption());
       energy.setSensor(sensor);
       energy.setTimestamp(energyDTO.getTimestamp());
       return energy;
    }

    public static EnergyDTO clone(EnergyDTO energyDTO) {
        EnergyDTO energy = new EnergyDTO();
        energy.setId(energyDTO.getId());
        energy.setConsumption(energyDTO.getConsumption());
        energy.setSensorId(energyDTO.getSensorId());
        energy.setTimestamp(energyDTO.getTimestamp());
        return energy;
    }
}
