package ro.tuc.ds2020.dtos.builders;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.entities.Device;

@Component
@RequiredArgsConstructor
public class DeviceBuilder {
    public static DeviceDTO toDTO(Device device) {
        DeviceDTO deviceDTO = new DeviceDTO();
        deviceDTO.setId(device.getId());
        deviceDTO.setDescription(device.getDescription());
        deviceDTO.setLocation(device.getLocation());
        deviceDTO.setMaxEnergyConsumption(device.getMaxEnergyConsumption());
        deviceDTO.setMediumEnergyConsumption(device.getMediumEnergyConsumption());
        deviceDTO.setPerson(device.getPerson());
        return deviceDTO;
    }

    public static Device toEntity(DeviceDTO deviceDTO) {
        Device device = new Device();
        device.setId(deviceDTO.getId());
        device.setDescription(deviceDTO.getDescription());
        device.setLocation(deviceDTO.getLocation());
        device.setMaxEnergyConsumption(deviceDTO.getMaxEnergyConsumption());
        device.setMediumEnergyConsumption(deviceDTO.getMediumEnergyConsumption());
        device.setPerson(deviceDTO.getPerson());
        return device;
    }
}
