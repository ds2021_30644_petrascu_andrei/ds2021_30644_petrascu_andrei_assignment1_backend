package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.entities.Sensor;

public class SensorBuilder {

    public static Sensor toEntity(SensorDTO sensorDTO) {
        Sensor sensor = new Sensor();
        sensor.setId(sensorDTO.getId());
        sensor.setDescription(sensorDTO.getDescription());
        sensor.setMaxValue(sensorDTO.getMaxValue());
        sensor.setCurrentValue(sensorDTO.getCurrentValue());
        sensor.setDevice(sensorDTO.getDevice());
        return sensor;
    }

    public static SensorDTO toDTO(Sensor sensor) {
        SensorDTO sensorDTO = new SensorDTO();
        sensorDTO.setId(sensor.getId());
        sensorDTO.setDescription(sensor.getDescription());
        sensorDTO.setMaxValue(sensor.getMaxValue());
        sensorDTO.setCurrentValue(sensor.getCurrentValue());
        sensorDTO.setDevice(sensor.getDevice());
        return sensorDTO;
    }

}
