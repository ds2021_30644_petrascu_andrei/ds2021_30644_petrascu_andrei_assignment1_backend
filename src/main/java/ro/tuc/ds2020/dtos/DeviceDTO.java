package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.tuc.ds2020.entities.Person;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDTO {
    private UUID id;
    private String description;
    private String location;
    private Integer maxEnergyConsumption;
    private Integer mediumEnergyConsumption;
    private Person person;
}
