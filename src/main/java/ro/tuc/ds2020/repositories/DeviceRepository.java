package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface DeviceRepository extends JpaRepository<Device, UUID> {

    @Query(value = "SELECT d FROM Device d WHERE d.person.id = :id")
    List<Device> getPersonDevices(@Param("id") UUID id);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM Device d WHERE d.person.id = :id")
    void deletePersonDevices(@Param("id") UUID id);

}
