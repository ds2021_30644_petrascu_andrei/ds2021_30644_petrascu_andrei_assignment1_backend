package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Sensor;

import java.util.List;
import java.util.UUID;

@Repository
public interface SensorRepository extends JpaRepository<Sensor, UUID> {
  /*  @Query(value = "SELECT s " +
            "FROM Sensor s INNER JOIN Device d ON s.device.id = d.id " +
            "where d.person.id = :id ")
    List<Sensor> getClientSensors(@Param("id") UUID id);*/

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM Sensor s WHERE s.device.id = :id")
    void deleteDeviceSensor(@Param("id") UUID id);

    @Query(value = "SELECT s FROM Sensor s INNER JOIN Device d ON s.device.id = d.id WHERE d.person.id = :id")
    List<Sensor> getPersonSensors(UUID id);

    @Query(value = "SELECT s FROM Sensor s WHERE s.device.id = :id")
    Sensor getDeviceSensor(UUID id);
}
