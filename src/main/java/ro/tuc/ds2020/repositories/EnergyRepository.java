package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.entities.Energy;

import java.util.List;
import java.util.UUID;

public interface EnergyRepository extends JpaRepository<Energy, UUID> {

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM Energy e WHERE e.sensor.id = :id")
    void deleteSensorEnergies(@Param("id") UUID id);

    @Query(value = "SELECT e FROM Energy e INNER JOIN Sensor s ON e.sensor.id = s.id WHERE s.device.id = :id")
    List<Energy> getDeviceEnergies(UUID id);
}
