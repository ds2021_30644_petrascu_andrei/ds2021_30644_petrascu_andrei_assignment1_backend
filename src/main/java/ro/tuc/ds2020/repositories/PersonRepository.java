package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PersonRepository extends JpaRepository<Person, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    /*Optional<Person> findByUsernameAndPassword(String username, String password);*/
    Optional<Person> findByUsername(String username);

    /**
     * Example: Write Custom Query
     */
    /*@Query(value = "SELECT p " +
            "FROM Person p " +
            "WHERE p.name = :name ")
    Optional<Person> findSeniorsByName(@Param("name") String name);*/

}
