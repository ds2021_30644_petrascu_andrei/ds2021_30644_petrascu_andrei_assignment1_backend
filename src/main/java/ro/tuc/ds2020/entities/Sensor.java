package ro.tuc.ds2020.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
public class Sensor {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    private String description;
    private Integer maxValue;
    private Integer currentValue;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(unique = true)
    private Device device;
}
