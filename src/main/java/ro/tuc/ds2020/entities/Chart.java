package ro.tuc.ds2020.entities;

import lombok.Data;

import java.util.List;

@Data
public class Chart {
    private String[] labels;
    private Integer[] data;
}
