package ro.tuc.ds2020.services;

import org.hibernate.cfg.CollectionSecondPass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.entities.Chart;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.PersonRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final PersonRepository personRepository;
    private final DeviceRepository deviceRepository;
    private final SensorRepository sensorRepository;

    @Autowired
    public PersonService(PersonRepository personRepository, DeviceRepository deviceRepository, SensorRepository sensorRepository) {
        this.personRepository = personRepository;
        this.deviceRepository = deviceRepository;
        this.sensorRepository = sensorRepository;
    }

    public List<PersonDTO> getAll() {
        List<Person> personList = personRepository.findAll();
        return personList.stream()
                .map(PersonBuilder::toDTO)
                .collect(Collectors.toList());
    }

    public UUID insert(PersonDTO personDTO) {
        Person person = PersonBuilder.toEntity(personDTO);
        person = personRepository.save(person);
        LOGGER.debug("Person with id {} was inserted in db", person.getId());
        return person.getId();
    }

    public PersonDTO findByUsername(String username) {
        Optional<Person> person = personRepository.findByUsername(username);
        if (!person.isPresent()) {
            LOGGER.error("Person with username {} was not found in db", username);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with username: " + username);
        }
        return PersonBuilder.toDTO(person.get());
    }

    public PersonDTO findById(UUID id) {
        Optional<Person> person = personRepository.findById(id);
        if (!person.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PersonBuilder.toDTO(person.get());
    }

    public void delete(UUID id) {
        deviceRepository.deletePersonDevices(id);
        personRepository.deleteById(id);
        LOGGER.debug("Person with id {} was deleted in db", id);
    }

    public List<DeviceDTO> getPersonDevices(UUID id) {
        List<Device> deviceList = deviceRepository.getPersonDevices(id);
        return deviceList.stream()
                .map(DeviceBuilder::toDTO)
                .collect(Collectors.toList());
    }

    public List<SensorDTO> getPersonSensors(UUID id) {
        List<Sensor> sensorList = sensorRepository.getPersonSensors(id);
        return sensorList.stream()
                .map(SensorBuilder::toDTO)
                .collect(Collectors.toList());
    }

    public Chart getEnergiesAsMap(UUID id) {

        List<Device> deviceList = deviceRepository.getPersonDevices(id);

        Map<String, Integer> energies = new HashMap<>();
        energies.put("00:00", 1);
        energies.put("01:00", 2);
        energies.put("02:00", 3);
        energies.put("03:00", 4);
        energies.put("04:00", 5);
        energies.put("05:00", 6);
        energies.put("06:00", 7);
        energies.put("07:00", 8);
        energies.put("08:00", 9);
        energies.put("09:00", 11);
        energies.put("10:00", 10);
        energies.put("11:00", 0);
        energies.put("12:00", 6);
        energies.put("13:00", 22);
        energies.put("14:00", 25);
        energies.put("15:00", 7);
        energies.put("16:00", 0);
        energies.put("17:00", 0);
        energies.put("18:00", 0);
        energies.put("19:00", 10);
        energies.put("20:00", 0);
        energies.put("21:00", 4);
        energies.put("22:00", 0);
        energies.put("23:00", 0);

        Chart chart = new Chart();

        Set<String> keys = energies.keySet();
        String[] labelsArray = keys.toArray(new String[0]);

        Collection<Integer> values = energies.values();
        Integer[] valuesArray = values.toArray(new Integer[values.size()]);

        chart.setLabels(labelsArray);
        chart.setData(valuesArray);

        return chart;
    }
}
