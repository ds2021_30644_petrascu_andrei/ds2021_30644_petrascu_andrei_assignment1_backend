package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.EnergyRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SensorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final SensorRepository sensorRepository;
    private final EnergyRepository energyRepository;

    @Autowired
    public SensorService(SensorRepository sensorRepository, EnergyRepository energyRepository) {
        this.sensorRepository = sensorRepository;
        this.energyRepository = energyRepository;
    }

    public List<SensorDTO> findAll() {
        List<Sensor> sensorList = sensorRepository.findAll();
        return sensorList.stream()
                .map(SensorBuilder::toDTO)
                .collect(Collectors.toList());
    }

    public UUID insert(SensorDTO sensorDTO) {
        Sensor sensor = SensorBuilder.toEntity(sensorDTO);
        sensor = sensorRepository.save(sensor);
        LOGGER.debug("Sensor with id {} was inserted in db", sensor.getId());
        return sensor.getId();
    }

    public SensorDTO findById(UUID id) {
        Optional<Sensor> sensor = sensorRepository.findById(id);
        if (!sensor.isPresent()) {
            LOGGER.error("Sensor with id {} was not found in db", id);
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + id);
        }
        return SensorBuilder.toDTO(sensor.get());
    }

    public void delete(UUID id) {
        energyRepository.deleteSensorEnergies(id);
        sensorRepository.deleteById(id);
        LOGGER.debug("Sensor with id {} was deleted in db", id);
    }
}
