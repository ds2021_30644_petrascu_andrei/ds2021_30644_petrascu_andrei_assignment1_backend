package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.services.SensorService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/sensors")
public class SensorController {

    private final SensorService sensorService;

    @Autowired
    public SensorController(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @GetMapping
    public ResponseEntity<List<SensorDTO>> getAll() {
        List<SensorDTO> dtos = sensorService.findAll();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<SensorDTO> getById(@PathVariable("id") UUID id) {
        SensorDTO sensorDTO = sensorService.findById(id);
        return new ResponseEntity<>(sensorDTO, HttpStatus.OK);
    }

   /* @GetMapping(value = "/getClientSensors/{id}")
    public ResponseEntity<List<SensorDTO>> getClientSensors(@PathVariable("id") UUID id) {
        List<SensorDTO> sensorDTOS = sensorService.getClientSensors(id);
        return new ResponseEntity<>(sensorDTOS, HttpStatus.OK);
    }*/

    @PostMapping
    public ResponseEntity<UUID> insert(@Valid @RequestBody SensorDTO sensorDTO) {
        UUID id = sensorService.insert(sensorDTO);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    private ResponseEntity<UUID> update(@PathVariable("id") UUID id, @RequestBody SensorDTO sensorDTO) {
        sensorService.insert(sensorDTO);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<UUID> delete(@PathVariable("id") UUID id) {
        sensorService.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

}
