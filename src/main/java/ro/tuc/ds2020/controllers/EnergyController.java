package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.Energy;
import ro.tuc.ds2020.repositories.EnergyRepository;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/energies")
public class EnergyController {
    private final EnergyRepository energyRepository;

    public EnergyController(EnergyRepository energyRepository) {
        this.energyRepository = energyRepository;
    }

    @PostMapping
    public ResponseEntity<UUID> insert(@Valid @RequestBody Energy energy) {
        Energy energy1 = energyRepository.save(energy);
        return new ResponseEntity<>(energy1.getId(), HttpStatus.CREATED);
    }
}
