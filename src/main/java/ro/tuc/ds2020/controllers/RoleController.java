package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.Role;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/roles")
public class RoleController {

    @GetMapping
    public ResponseEntity<List<Role>> getAll() {
        List<Role> roleList = Arrays.asList(Role.values());
        return new ResponseEntity<>(roleList, HttpStatus.OK);
    }

}
