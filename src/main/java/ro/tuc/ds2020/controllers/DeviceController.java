package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Energy;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.services.DeviceService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/devices")
public class DeviceController {

    private final DeviceService deviceService;

    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping
    public ResponseEntity<List<DeviceDTO>> getAll() {
        List<DeviceDTO> deviceDTOS = deviceService.findAll();
        return new ResponseEntity<>(deviceDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DeviceDTO> getById(@PathVariable("id") UUID id) {
        DeviceDTO deviceDTO = deviceService.findById(id);
        return new ResponseEntity<>(deviceDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/getDeviceSensor/{id}")
    public ResponseEntity<SensorDTO> getDeviceSensor(@PathVariable("id") UUID id) {
        SensorDTO sensorDTO = deviceService.getDeviceSensor(id);
        return new ResponseEntity<>(sensorDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/getDeviceEnergies/{id}")
    public ResponseEntity<List<Energy>> getDeviceEnergies(@PathVariable("id") UUID id) {
        List<Energy> energyList = deviceService.getDeviceEnergies(id);
        return new ResponseEntity<>(energyList, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UUID> insert(@RequestBody DeviceDTO deviceDTO) {
        UUID deviceID = deviceService.insert(deviceDTO);
        return new ResponseEntity<>(deviceID, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    private ResponseEntity<UUID> update(@PathVariable("id") UUID id, @RequestBody DeviceDTO deviceDTO) {
        //deviceDTO.setId(id);
        deviceService.insert(deviceDTO);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<UUID> delete(@PathVariable("id") UUID id) {
        deviceService.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

}
