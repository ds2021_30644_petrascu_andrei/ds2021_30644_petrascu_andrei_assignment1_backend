package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.services.PersonService;

@RestController
@CrossOrigin
@RequestMapping(value = "/login")
public class LoginController {

    private final PersonService personService;

    @Autowired
    public LoginController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping
    public ResponseEntity<PersonDTO> login(@RequestBody PersonDTO personDetailsDTO) {
        PersonDTO user = personService.findByUsername(personDetailsDTO.getUsername());
        if(personDetailsDTO.getPassword().equals(user.getPassword())){
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

}
