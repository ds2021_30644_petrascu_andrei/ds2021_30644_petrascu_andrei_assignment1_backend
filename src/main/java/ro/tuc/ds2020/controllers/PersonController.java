package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.entities.Chart;
import ro.tuc.ds2020.services.PersonService;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin
@RequestMapping(value = "/persons")
public class PersonController {
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public ResponseEntity<List<PersonDTO>> getAll() {
        List<PersonDTO> dtos = personService.getAll();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /*@GetMapping(value = "/{id}")
    public ResponseEntity<PersonDTO> getById(@PathVariable("id") UUID id) {
        PersonDTO personDTO = personService.findById(id);
        return new ResponseEntity<>(personDTO, HttpStatus.OK);
    }*/

    @GetMapping(value = "/{username}")
    public ResponseEntity<PersonDTO> getByUsername(@PathVariable("username") String username) {
        PersonDTO personDTO = personService.findByUsername(username);
        return new ResponseEntity<>(personDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/getPersonDevices/{id}")
    public ResponseEntity<List<DeviceDTO>> getPersonDevices(@PathVariable("id") UUID id) {
        List<DeviceDTO> deviceDTOS = personService.getPersonDevices(id);
        return new ResponseEntity<>(deviceDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/getPersonSensors/{id}")
    public ResponseEntity<List<SensorDTO>> getPersonSensors(@PathVariable("id") UUID id) {
        List<SensorDTO> sensorDTOS = personService.getPersonSensors(id);
        return new ResponseEntity<>(sensorDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/getEnergiesAsMap/{id}")
    public ResponseEntity<Chart> getEnergiesAsMap(@PathVariable("id") UUID id) {
        Chart chart = personService.getEnergiesAsMap(id);
        return new ResponseEntity<>(chart, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UUID> insert(@Valid @RequestBody PersonDTO personDTO) {
        UUID id = personService.insert(personDTO);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    private ResponseEntity<UUID> update(@PathVariable("id") UUID id, @RequestBody PersonDTO personDTO) {
        personService.insert(personDTO);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<UUID> delete(@PathVariable("id") UUID id) {
        personService.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

}
