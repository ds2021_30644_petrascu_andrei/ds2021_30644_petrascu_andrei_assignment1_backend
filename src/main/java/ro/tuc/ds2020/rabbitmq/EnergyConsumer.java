package ro.tuc.ds2020.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.EnergyDTO;
import ro.tuc.ds2020.dtos.builders.EnergyBuilder;
import ro.tuc.ds2020.entities.Energy;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.EnergyRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.util.UUID;

@Component
public class EnergyConsumer {

    @Autowired
    private EnergyRepository energyRepository;
    @Autowired
    SensorRepository sensorRepository;

    private EnergyDTO currentEnergy;
    private boolean firstTimeHere = true;
    private Sensor sensor;

    @RabbitListener(queues = MQConfig.QUEUE)
    public void listener(EnergyDTO energy) {
        System.out.println(energy);
        Integer peak;
        if (firstTimeHere == true) {
            peak = energy.getConsumption();
            sensor = sensorRepository.findById(UUID.fromString("51108c19-ae9d-482a-aa97-9a4a77097aa1")).get();
            firstTimeHere = false;
        } else {
            peak = energy.getConsumption() - currentEnergy.getConsumption();
        }
        currentEnergy = EnergyBuilder.clone(energy);

        // notify the client that a bug was found
        if (peak > sensor.getMaxValue()) {
            System.out.println("Sensor current value > max value");
        }

        Energy energyToDB = EnergyBuilder.toEntity(energy, sensor);
        System.out.println("to DB: " + energyToDB.toString());
        energyRepository.save(energyToDB);

    }
}
